import React, { Component } from 'react'

export default class Async extends Component {
  state={
    count:0,
    ref:null,
    count2:0,
    ref2:null,
    name:"Rakesh"
  };
  handleClick=(some)=>
  {
    
    this.setState({ref:setInterval(
      ()=>{this.setState({count:this.state.count+1});
    console.log(this.state.count)},some.value)});
    console.log("button clicked");
  }
  handleClick2=()=>
  {
  clearInterval(this.state.ref);
  }

  handleClick3=(some)=>
  {
    
    this.setState({ref2:setTimeout(
      ()=>{
        console.log("ref start :"+this.state.ref2);
    this.setState({ name:"Ramesh"});
  },some.value)});
    console.log("button clicked");
    console.log(this.state.ref2);
  }
  handleClick4=()=>
  {

    console.log(this.state.ref2);
  clearTimeout(this.state.ref2);
  console.log("handleClicked");

  }

  render() {
    return (
      <div>
        <h1>setInterval Example</h1>
        <h1>Hello count {this.state.count}</h1>
      <input type="text"  id="some"/>
      <button className='btn btn-primary ' onClick={()=>this.handleClick(document.getElementById("some"))}>click me</button>
      <button className='btn btn-danger' onClick={this.handleClick2}>click to stop</button>
      
      <hr/>

      <h2>setTimeout Example</h2>
      <h1>Hello user you have set {this.state.count2} as  time interval..</h1>
      <input type="text"  id="some2"/>
      <button className='btn btn-primary ' onClick={()=>this.handleClick3(document.getElementById("some2"))}>click me</button>
      <button className='btn btn-danger' onClick={this.handleClick4}>click not to wait</button>
      <h2>Hello {this.state.name}</h2>
      </div>
      
    )
  }
}
